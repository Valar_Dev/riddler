package Dev.Riddler;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.Material;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

public class CmdGiveItem implements CommandExecutor {

	public boolean onCommand(CommandSender sender, Command cmd, String commandLabel, String[] args) {
		if(cmd.getName().equalsIgnoreCase("RiddlerWand")){
			if(sender.hasPermission("riddler.setTrophy"))
			{
				ItemStack item = new ItemStack(Material.ARROW);
				ItemMeta meta = item.getItemMeta();
				meta.setDisplayName("Reward Setter");
				List<String> lore = new ArrayList<String>();
				lore.add("This is 95% legit");
				meta.setLore(lore);
				item.setItemMeta(meta);
				if(sender instanceof Player){
					((Player)sender).getInventory().addItem(item);
				}
				return true;
			}
			else
				sender.sendMessage("You dont have permission to do that.");
		}
		return false;
	}
	
}