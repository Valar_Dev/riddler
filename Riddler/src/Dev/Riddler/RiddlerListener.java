package Dev.Riddler;

import net.md_5.bungee.api.ChatColor;

import org.bukkit.Material;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

public class RiddlerListener implements Listener {

	@EventHandler
	public void onPlayerInteract(PlayerInteractEvent event) {
		if(event.getPlayer().getItemInHand().getType().equals(Material.ARROW))
		{
			ItemStack item = event.getPlayer().getItemInHand();
			if(item.hasItemMeta())
			{
				ItemMeta meta = item.getItemMeta();
				if(meta.hasLore()&&meta.hasDisplayName())
				{
					if(meta.getDisplayName().equals("Reward Setter")){
						if(meta.getLore().get(0).equals("This is 95% legit")){
							if(event.getPlayer().hasPermission("riddler.setTrophy")){
								if(event.getAction() ==Action.LEFT_CLICK_BLOCK){
									Riddler.rewardLocations.add(event.getClickedBlock().getLocation());
									event.getPlayer().sendMessage(ChatColor.GREEN + "Trophy Location added!");
								}
								else if(event.getAction()==Action.RIGHT_CLICK_BLOCK){
									if(Riddler.rewardLocations.contains(event.getClickedBlock().getLocation())){
										Riddler.rewardLocations.remove(event.getClickedBlock().getLocation());
										event.getPlayer().sendMessage("Location Removed!");
									}
									else{
										event.getPlayer().sendMessage("That wasn't a reward location");
									}
								}
							}
						}
					}
				}
			}
		}
	}
	

	@EventHandler
	public void onBlockBreak(BlockBreakEvent event) {
		if(Riddler.rewardLocations.contains(event.getBlock().getLocation())){
			//Check cooldown and give reward
		}
	}
}