package Dev.Riddler;

import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.util.List;



import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.plugin.java.JavaPlugin;


public class Riddler extends JavaPlugin {

	public static List<Location> rewardLocations = new ArrayList<Location>();
	
	@Override
	public void onEnable(){
		checkDirectory();
		loadFile();
		getCommand("riddlerwand").setExecutor(new CmdGiveItem());
		getServer().getPluginManager().registerEvents(new RiddlerListener(), this);
	}
	
	@Override
	public void onDisable(){
		saveFile();
	}
	
	public void checkDirectory(){
		File Dir = new File(this.getDataFolder()+"");

        if (!Dir.exists())
        {
            if (!Dir.mkdirs())
            {
                getLogger().severe("Critical - Failed to create Riddlers dir!");
            }
        }
	}
	
	@SuppressWarnings("resource")
	public void loadFile(){
		File file = new File(this.getDataFolder()+"locations.txt");
		if(file.exists()){
			try {
				BufferedReader br = new BufferedReader(new FileReader(file));
				String s = br.readLine();
				while(!s.equals("_")){
					String[] data = s.split(",");
					rewardLocations.add(new Location(Bukkit.getWorld(data[0]), Double.parseDouble(data[1])
							,Double.parseDouble(data[2]),Double.parseDouble(data[3])));
					
					s = br.readLine();
				}
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
		}
	}
	
	public void saveFile(){
		File file = new File(this.getDataFolder()+"locations.txt");
		if(file.exists()){
			file.delete();
		}

		try {
			file.createNewFile();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		try {
			BufferedWriter bw = new BufferedWriter(new FileWriter(file.getPath()));
			for(Location l : rewardLocations){
				bw.write(l.getWorld().getName()+","+l.getBlockX()+","+l.getBlockY()+","+l.getBlockZ());
				bw.newLine();
			}
			bw.write("_");
			bw.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}